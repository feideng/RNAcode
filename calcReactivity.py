import sys
import os
import math
import HTSeq
import pysam
import utils
import countReads


def calculateReactivity(plus_almnt_file, minus_almnt_file, paired_end, output_prefix, method="mle"):
    """
    Calculate reactivity from plus and minus chanel using Ding et al. Nature 2014 model.
    
    Args:
        plus_almnt_file: HTSeq.Alignment, Alignment file handler pointing to plus chanel.
        minus_almnt_file: HTSeq.Alignment, Alignment file handler pointing to minus chanel.
        references: dict, Dictionary for reference sequences, mapping from reference_name to the length of reference sequence.
        paired_end: bool, True if input files contain paired_end data.
        output_file: string, File name for writing output.

    Returns:
        A dictionary containing calculated reactivities.    
    """
    output_reactivity_file = output_prefix + "_raw.reac"
    output_plus_count_file = output_prefix + "_plus.cnt"
    output_minus_count_file = output_prefix + "_minus.cnt"

    references = utils.getReference(plus_almnt_file)
    plus_counts, plus_coverage = countReads.calculateStopCounts(plus_almnt_file, references, paired_end)
    minus_counts, minus_coverage = countReads.calculateStopCounts(minus_almnt_file, references, paired_end)
    countReads.writeStopCounts(plus_counts, plus_coverage, references, output_plus_count_file)
    countReads.writeStopCounts(minus_counts, minus_coverage, references, output_minus_count_file)

    raw_reac = None
    if method.lower() == "mle":
        raw_reac = calculateReactivityMLE(plus_counts, plus_coverage, minus_counts, minus_coverage, references)
    else:
        raw_reac = calculateReactivityDing(plus_counts, minus_counts, references)
    utils.writeReactivities(raw_reac, output_reactivity_file)
    return raw_reac 


def calculateReactivityMLE(plus_counts, plus_coverage, minus_counts, minus_coverage, references):
    """
    Calculate reactivities using maximum likelihood model described in the following paper:

    Shraon Aviran et al. RNA structure characterization from chemical mapping experiments. 2011

    Args:
        plus_counts: GenomicArray, Stop counts for plus chanel.
        plus_coverage: GenomicArray, Coverage for plus chanel.
        minus_counts: GenomicArray, Stop counts for minus chanel.
        minus_coverage: GenomicArray, Coverage for minus chanel.
        references: dict, Dictionary for reference sequences, mapping from reference_name to the length of reference sequence.

    Returns:
        A dictionary that contains the calculated beta for each reference sequence, as indicated by <references>.
    """
    beta = {}
    for chrome in references:
        beta[chrome] = []
    for chrome, counts_vector in plus_counts.chrom_vectors.items():
        chrom_size = references[chrome]
        for index in range(1, chrom_size):
            pos = HTSeq.GenomicPosition(chrome, index, ".")
            if plus_coverage[pos] == 0 or minus_coverage[pos] == 0:
                beta[chrome].append(0.0)
                continue
            plus_ratio = 1.0 * plus_counts[pos] / plus_coverage[pos]
            minus_ratio = 1.0 * minus_counts[pos] / minus_coverage[pos]
            if abs(minus_ratio - 1.0) < 0.00000000001:
                beta[chrome].append(0.0)
            else:
                the_beta = (plus_ratio - minus_ratio) / (1 - minus_ratio)
                beta[chrome].append(max(the_beta, 0))
    return beta


def calculateReactivityDingHelper(stop_counts, references):
    """
    Calculate normalized count (either plus or minus chanel) using Ding et al. Nature 2014 model.

    Args:
        stop_counts: GenomicArray, Stop counts
        references: dict, Dictionary for reference sequences, mapping from reference_name to the length of reference sequence.
    
    Returns:
        A dictinoary that contains normalized counts data.
    """
    norm_count_dict = {}
    for chrome in references:
        tmp = None
        norm_count_dict[chrome] = tmp
    for chrome, counts_vector in stop_counts.chrom_vectors.items():
        chrom_size = references[chrome]
        log_count = []
        for index in range(0, chrom_size):
            val = math.log(stop_counts[HTSeq.GenomicPosition(chrome, index, ".")] + 1)
            log_count.append(val)
        avg_count = sum(log_count) / chrom_size
        norm_count = [float(-999)] * chrom_size
        if not abs(avg_count) < 0.00000000001:
            norm_count = [x / avg_count for x in log_count]
        norm_count_dict[chrome] = norm_count
    return norm_count_dict


def calculateReactivityDing(plus_counts, minus_counts, references):
    """
    Calculate reactivity from plus and minus chanel using Ding et al. Nature 2014 model.
    
    Args:
        plus_counts: GenomicArray, Stop counts for plus chanel.
        minus_counts: GenomicArray, Stop counts for minus chanel.
        output_file: string, File name for writing output.

    Returns:
        A dictionary containing calculated reactivities.    
    """
    raw_reac = {}
    for chrome in references:
        raw_reac[chrome] = []
    normalized_counts_plus = calculateReactivityDingHelper(plus_counts, references)
    normalized_counts_minus = calculateReactivityDingHelper(minus_counts, references)
    
    for chrome, counts_vector in plus_counts.chrom_vectors.items():
        chrom_size = references[chrome]
        for index in range(1, chrom_size):
            pos = HTSeq.GenomicPosition(chrome, index, ".")
            reactivity = max(normalized_counts_plus[chrome][index] - normalized_counts_minus[chrome][index], 0)
            if (reactivity < 0.00000000001):
                raw_reac[chrome].append(0.0)
            else:
                raw_reac[chrome].append(reactivity)             
    return raw_reac    






