# simulate paired-end reads for plus chanel
./wgsim -e0 -d200 -N1000 -140 -240 -r0 -R0 genome.fa p1.fastq p2.fastq

# simulate paired-end reads for minus chanel
./wgsim -e0 -d200 -N1000 -140 -240 -r0 -R0 genome.fa m1.fastq m2.fastq
