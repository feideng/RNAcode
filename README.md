# RNAcode

A pipeline to process structure probing data.


# Installation

Install samtools, bowtie/bowtie2, HTSeq, pysam before running the pipeline.

## Install samtools
1. Download and install samtools (Required version: >=1.3.1), see http://www.htslib.org/download/.
2. Add the directory where the executable program reside to the PATH variable (see Add directory to PATH section).

Note: samtools updated its command line options, so lower versions may not be able to work properly.

## Install bowtie/bowtie2
1. Download and install the bowtie version you need.
2. Add the directory where the executable program reside to the PATH variable (see **Add directory to PATH** section below).

## Install pysam
If you have root permission, type:
```
pip install pysam
```
Otherwise, type:
```
pip install --user pysam
```
to install pysam for the current user only.

See instructions from http://pysam.readthedocs.io/en/latest/installation.html.


## Install HTSeq

See instructions from http://www-huber.embl.de/users/anders/HTSeq/doc/install.html.

# Add directory to PATH

## On Linux
1. Open the terminal and type: 
```
vim ~/.bashrc
```
2. Add the following line to .bashrc:
```
export PATH=/The_Directory_to_be_added/:$PATH
```

3. Save the file and quite.

4. Type the following command in terminal:
```
vim ~/.bash_profile
```

5. Add the following lines into .bash_profile:
```
if [ -f ~/.bashrc ]; then
   source ~/.bashrc
fi
```

6. Save the file and quite.

7. Type the following in terminal.
```
source ~/.bash_profile
```

## On MacOS X
1. Open terminal, and type: 
```
vim ~/.bash_profile
```
2. Add the following line to .bash_profile:
```
export PATH=/The_Directory_to_be_added/:$PATH
```

3. Save the file and quite.

4. Type the following in terminal:
```
source ~/.bash_profile
```

# Running the pipeline

1. Open file config.cfg and do the corresponding settings.

2. Run the pipeline by typing the following command in terminal:
```
python RNAcode.py config.cfg
```

# Output files
After sucessfully execution of the pipeline, four folders will be created:

* bowtie_index:
	+ bowtie/bowtie2 index
	+ ref_transcripts.fa: if a gene annotation file is given, transcripts will be extracted from the genome sequence and written into this file.

* aligned_reads: The alignment information are given here.
	+ plus_alignment.bam: bowtie/bowtie2 alignment for plus chanel.
	+ minus_alignment.bam: bowtie/bowtie2 alignment for minus chanel.
	+ my_plus.algn: the first/last alignment position for each read.
	+ my_plus_sorted.algn: sorted my_plus.algn based on alignment position.
	+ my_minus.algn: the first/last alignment position for each read.
	+ my_minus_sorted.algn: sorted my_minus.algn based on alignment position.

* raw_reactivity:
	+ my_plus.cnt: stop counts and local coverage for plus chanel. Each transcript is represented using three lines: the first line show the transcript name, the next two lines give stop counts and local coverage for each position.
	+ my_minus.cnt: stop counts and local coverage for minus chanel.
	+ my_raw.reac: raw reactivities calculated using either the MLE formula or Yiliang's structure-seq formula. Default: MLE.

* final_reactivity:
	+ my.reac: normalized reactivities. Available options: 2-8% normalization and boxplot normalization. Default: 2-8% normalization.

# A little bit more on the pipeline

## Adapter Trimming
Adapter trimming is currently not included as part of the pipeline. There are too many data out there, with different library preparation methods. Instead of writing lengthy code to handle all these differences in the pipeline, I personally believe it's better to do the trimming separately before using the pipeline. This will not only make the pipeline logicly simpler but also give end-user more freedom to determine the parameters for adapter trimming based on their specific requirements.


For paired-end libraries, I personally use trimmomatic right now.


### The pipeline consists of the following four major components.

## Read mapping

Currently, **bowtie** and **bowtie2** are allowed. Users can choose which one to run in the configuration file. There is another version (checkout using "git checkout start_end_algn_pos") which accepts an aligned SAM/BAM file as input. Later I will merge it back to the master branch.

When bowtie is used, the default parameters are as follows:

```
bowtie --threads <num_threads> --chunkmbs 1024 --best <basename_of_bowtie_index> -1 <m1> -2 <m2> -S <output.sam>
```

When bowtie2 is used, the default parameters are as follows:
```
bowtie2 --threads <num_threads> -x <basename_of_bowtie2_index> -1 <m1> -2 <m2> -S <output.sam>
```

If user specifies any other options in the configuration file, it will automatically used for mapping. 

* Notes: 
	+ bowtie/bowtie2 index will be build before the actual mapping. If both a reference genome (in fasta or multi fasta format) and a gene annotation file (in .gff, .gtf, .gff3) are given, the transcripts will be extracted from the genemoe sequence first, which are then using the input to build bowtie index. If only the reference genome is given, it will be used to build bowtie index directly.

	+ In the current implementation of parsing transcripts from a gene annotation file, the pipeline will look for rows with feature type "transcript" (the 3rd column of a gtf file). Sometimes you may want other features (sometimes a gtf file does not have feature type 'transcript', but others like "exome"), you can change that in the  *extractTranscripts* function of parseGeneAnnotation.py file. Later version will have an option to specify that in the configuration file.

## Computing stop counts and local coverage 

A pair of reads p1 and p2 are excluded from the following analyses if they are not mapped the same transcript (either one of them is not mapped or both are mapped but not to the same transcript). 

For each correctly mapped pair p1 and p2, assume they are mapped to the transcript region (s1, e1)  (note that s1 and e1 are the first and last position of the alignment) and (s2, e2) respectively. Let s = min(s1, s2), e = max(e1, e2). Then the stop count for s is increased by 1. And the local coverage for each s <= i <= e is also increased by 1. **Note:** in this step we do not account for the fact the actual modification site is actually 1-nt upstream of the corresponding RT stop site. It is handled in the next step.

Now, we have the stop count and local coverage value for each position. To account for the fact the actual modification site is actually 1-nt upstream of the corresponding RT stop site, we then shift all these values 1 position to the left. In other words, for each position i, its stop count and local coverage are actually the values for position i+1 obtained earlier. Note that the n-th position of a transcript of length n has no meaningful values  and is therefore ignored. 

Up to now, we have computed the stop counts and local coverages. 

## Computing raw reactivities

Two options are currently available: MLE formula by Sharon and the Ding formula by Yiliang used in the structure-seq paper.

## Normalizing raw reactivities

In this step, raw reactivities are normalized using one of the two methods: 2-8% normalization or boxplot normalization.


# Instructions for adapter trimming

You can use either trimmomatic or cutadapt to do adapter trimming.

## How to use trimmomatic

Given a paired-end library, **mSHAPE-rep1-GCCAAT-R1.fastq** and **mSHAPE-rep1-GCCAAT-R2.fastq**, use the following steps to do adapter trimming and quality trimming.

Step 1. Put **trimmomatic-0.36.jar** and **TruSeq3-PE-2.fa** into the same folder where the fastq files reside. TruSeq3-PE-2.fa contains information about TruSeq adapters and it can be found from the **adapters** subfolder after you download trimmomatic.

Step 2. Run the following command to do adapter trimming.
```
java -jar trimmomatic-0.36.jar PE -threads 1 mSHAPE-rep1-GCCAAT-R1.fastq  mSHAPE-rep1-GCCAAT-R2.fastq  -baseout mSHAPE-rep1 ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10:8:true TRAILING:20  MINLEN:21 
```

Notes:
* threads 1 indicates the number of threads to be used. Change it based on the resources you have.
* TRAILING:20 means low-quality nucleotides at the 3'-end (<20) will be trimmed.
* MINLEN:21 means any read with a length < 21 will not be included.


After this step, four files will be generated: 
* mSHAPE-rep1_1P: paired forward reads after adapter trimming.
* mSHAPE-rep1_1U: Not useful.
* mSHAPE-rep1_2P: paired reverse reads after adapter trimming.
* mSHAPE-rep1_2U: Not useful.

Step 3. Remove the NNN from forward reads.
```
java -jar trimmomatic-0.36.jar SE -threads 1  mSHAPE-rep1_1P mSHAPE-rep1_R1.fastq.gz HEADCROP:3 
```

Step 4. Remove the NNNNNN from reverse reads.
```
java -jar trimmomatic-0.36.jar SE -threads 1 mSHAPE-rep1_2P mSHAPE-rep1_R2.fastq.gz HEADCROP:6 
```

Step 5. Delete tempary files.
```
rm mSHAPE-rep1_1U mSHAPE-rep1_2U mSHAPE-rep1_1P mSHAPE-rep1_2P
```

As a result, mSHAPE-rep1_R1.fastq.gz and mSHAPE-rep1_R2.fastq.gz will be ready for the alignment process.


You can always adjust the parameters used for running trimmomatic based on library preparation. Read the instructions for more details (http://www.usadellab.org/cms/?page=trimmomatic) and manual (http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/TrimmomaticManual_V0.32.pdf).


## How to use cutadapt
Step 1. Remove 3' adaptor from r1.fastq and r2.fastq.
```
cutadapt -a AGATCGGAAGAGCACACGTCTGAACTCCAGTCACTGACCAATCTCGTATGCCGTCTTCTGCTTG  -o r1_trimmed.fastq r1.fastq
cutadapt -a AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT -o r2_trimmed.fastq r2.fastq
```

Step 2. Remove the 3 random nucleotide at the 5' end of r1.
```
cutadapt -u 3 -o r1_cutNNN.fastq r1_trimmed.fastq
```

Step 3. Remove the 6 random nucleotide at the 5'end of r2 (from the direction of the reverse primer).
```
cutadapt -u 6 -o r2_cut6N.fastq r2_trimmed.fastq
```



